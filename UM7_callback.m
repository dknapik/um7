function UM7_callback(um7port, ~)
% Callback function for handling serial communication with DUT in gimballed
% temperature chamber
%
% Reads all available data and passes it to data structures
%
%


% Create persistent variable for storing previous data collects
persistent stored_data;

if isempty(stored_data)
    stored_data = [];
end

% If port is open, check to see if new data is available.  If so, read it
% in and do stuff with it
% if ~strcmp(DUT_port.status,'open')
% 	return;
% end

if ~um7port.NumBytesAvailable
	return;
end

       
% New data is available.  Read it in!
try
	new_data = read(um7port,um7port.NumBytesAvailable, 'UINT8');
catch exception
	fprintf('ERROR: Failed to read from DUT port.  Shutting down.\n');
	cleanup();
	return;
end

data_array = [stored_data;new_data];

% Go through the data and keep parsing it as long as there are new
% packets available.  The 'parse_serial_data' only parses a single packet
% at a time.
packet.NewPacket = 1;
while packet.NewPacket == 1
	[packet,data_array] = parse_serial_data(data_array);
	got_batch_data = 0;
	% Report bad checksum if appropriate
	if packet.BadChecksum
% 		fprintf('ERROR: Bad Checksum.\n');
	end
	
	% If packet was received, do stuff with it
	if packet.NewPacket == 1
		% HANDLE RAW BATCH DATA PACKET
		if packet.Address == 86 && packet.Length == 51
			% Extract the batch data
            gyro_x = typecast( uint8(packet.data(2:-1:1)), 'int16' );
            gyro_y = typecast( uint8(packet.data(4:-1:3)), 'int16' );
            gyro_z = typecast( uint8(packet.data(6:-1:5)), 'int16' );
            %7-8 reseved
            time1 = typecast( uint8(packet.data(12:-1:9)), 'single' );
            
			acc_x = typecast( uint8(packet.data(14:-1:13)), 'int16' );
			acc_y = typecast( uint8(packet.data(16:-1:15)), 'int16' );
            acc_z = typecast( uint8(packet.data(18:-1:17)), 'int16' );
            %19-20 reserved
            time2 = typecast( uint8(packet.data(24:-1:21)), 'single' );
            
            mag_x = typecast( uint8(packet.data(26:-1:25)), 'int16' );
			mag_y = typecast( uint8(packet.data(28:-1:27)), 'int16' );
            mag_z = typecast( uint8(packet.data(30:-1:29)), 'int16' );
            %31-32 reserved
            time3 = typecast( uint8(packet.data(36:-1:33)), 'single' );
            
            temp = typecast( uint8(packet.data(40:-1:37)), 'single' );
            time4 = typecast( uint8(packet.data(44:-1:41)), 'single' );
            
			got_batch_data = 1;
% 			fprintf('%d\t%d\t%d\n',gyro_x,gyro_y,gyro_z);
		end 

		% HANDLE TEMPERATURE DATA PACKET
% 		if packet.Address == 118
% 			temperature = typecast( flipud(uint8(packet.data(1:4))), 'single' );
% 			got_temperature_data = 1;
% % 			fprintf('%3.2f\n',temperature);
% 		end

        

		% If we received batch data, log it to
		% the workspace if logging is enabled.
		if  got_batch_data
% 			got_temperature_data = 0;
			got_batch_data = 0;
            
            % If first batch data logging create labels.
            if ~um7port.UserData.samplesCollected 
                um7port.UserData.data.signals(1).label = 'gyroscope';
                um7port.UserData.data.signals(2).label = 'accelerometer';
                um7port.UserData.data.signals(3).label = 'magnetometer';
                um7port.UserData.data.signals(4).label = 'temperature';
            end
            
 			if um7port.UserData.loggingData == 1 
				if um7port.UserData.samplesCollected < um7port.UserData.NoOfSamples
			
% 					GDATA.DUT_data(GDATA.DUT_samples_collected,:) = single([time,gyro_x,gyro_y,gyro_z,acc_x,acc_y,acc_z,time2,mag_x,mag_y,mag_z,time3,temp,time4]);
                    um7port.UserData.samplesCollected = um7port.UserData.samplesCollected + 1;
                    um7port.UserData.data.time(um7port.UserData.samplesCollected,:) = [time1, time2, time3, time4];
                    um7port.UserData.data.signals(1).values(um7port.UserData.samplesCollected,:) = single([gyro_x, gyro_y, gyro_z]);
                    um7port.UserData.data.signals(2).values(um7port.UserData.samplesCollected,:) = single([acc_x, acc_y, acc_z]);
                    um7port.UserData.data.signals(3).values(um7port.UserData.samplesCollected,:) = single([mag_x, mag_y, mag_z]);
                    um7port.UserData.data.signals(4).values(um7port.UserData.samplesCollected,:) = temp;

				else
					fprintf('Maximum datapoints were collected.  Logging disabled.\n');
					um7port.UserData.loggingData = 0;
                    configureCallback(um7port, "off");
				end
 			end
		end

	end

end % End of "while new packet found"

% Copy whatever is left in the data array to our local persistent
% variable so that we can use it next time we go through the array.
stored_data = data_array;

% We are done here!
