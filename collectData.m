%% Initializes serial port objects and assigns message handlers

% Before running, run cleanup function to close ports and remove timers
% that may have been running before.
cleanup();

% Clear workspace and close any open figures
close all;
clear;

port_string = 'COM3';
% global GDATA;

% Create serial port objects and open it
um7port = serialport(port_string,115200,'DataBits',8,'Parity','None');
device_opened = 1;

% Create structure for User Data
um7port.UserData = struct("samplesCollected",0, ...
    "loggingData", 1, ...
    "NoOfSamples", 2000, ...
    "data", struct("time",[],"signals",struct("values",[],"label",[])) );

try
    % Flush serialport object to remove any old data
    flush(um7port);
    % Create callback function
    configureCallback(um7port,"byte",7,@UM7_callback)
catch ME
    device_opened = 0;
    fprintf(1,'ERROR: Failed to open UM7 port.  Is the port already open?\n');
end

% If we failed to open any of the individual ports, quit
if (device_opened == 0)
    cleanup();
end

% Get rid of variables that we aren't using anymore
% clear('DUT_board_opened','DUT_opened','control_port_opened','DUT_control_timer_period');

% clear um7port;

%% Print some data received
if um7port.UserData.samplesCollected == um7port.UserData.NoOfSamples
    for i = 1:4
        subplot(4,1,i)
        plot(um7port.UserData.data.time(:,i),um7port.UserData.data.signals(i).values);
        title(um7port.UserData.data.signals(i).label)
    end
end

